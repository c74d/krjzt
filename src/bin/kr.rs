
#[macro_use]
extern crate error_chain;

extern crate clap;
extern crate krjzt;

use krjzt::ResultExt;

quick_main!(|| -> krjzt::Result<()> {
    let args = clap::App::new("krjzt kr")
        .version(option_env!("CARGO_PKG_VERSION").unwrap_or(
            "<unknown version>",
        ))
        .author(option_env!("CARGO_PKG_AUTHORS").unwrap_or(""))
        .about(
            "Interface for running krjzt commands from outside the krjzt system",
        )
        .get_matches();

    let _ = args;

    Ok(())
});
