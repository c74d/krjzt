use self::chars_and_bytes::*;
pub use self::err::*;
pub use self::input_position::*;
pub use self::input_span::*;
use super::InputResourceDescriptor;
use std::i8;
use std::io;

mod chars_and_bytes;
mod err;
mod input_position;
mod input_span;

pub mod phases;

#[cfg(test)]
mod tests;

type IOByte = u8;
type IOByteResult<R> = <io::Bytes<R> as Iterator>::Item;

type IndentLevel = i8;
type IndentChange = i8;

pub fn lex<R>(
    raw_bytes_iter: io::Bytes<R>,
    input_resrc: InputResourceDescriptor,
) -> phases::last::InputTokens<R>
where
    R: io::Read,
{
    let bytes = phases::bytes::InputBytes::new(raw_bytes_iter, input_resrc);
    let tokens_alpha = phases::alpha::InputTokens::from_bytes(bytes);
    let tokens_beta = phases::beta::InputTokens::from_alpha(tokens_alpha);
    let tokens_gamma = phases::gamma::InputTokens::from_beta(tokens_beta);
    tokens_gamma
}
