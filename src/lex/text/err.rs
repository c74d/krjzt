use super::InputPosition;
use super::InputSpan;
use std::io;
use std::string;

error_chain! {
    errors {
        IOError(err: io::Error, pos: InputPosition) {}

        Utf8Error(err: string::FromUtf8Error, span: InputSpan) {}

        StraySpaceChar(pos: InputPosition) {}

        StrayTabChar(pos: InputPosition) {}

        ExcessiveIndentation(pos: InputPosition) {}

        ByteCountOverflow(prev_pos: InputPosition) {
            description("the count of bytes in an input resource overflowed the capacity of its \
                         storage field")
        }

        LineCountOverflow(prev_pos: InputPosition) {
            description("the count of lines in an input resource overflowed the capacity of its \
                         storage field")
        }

        ColumnCountOverflow(prev_pos: InputPosition) {
            description("the count of columns in an input line overflowed the capacity of its \
                         storage field")
        }
    }
}
