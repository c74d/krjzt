use super::InputPosition;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct InputSpan {
    pub start: InputPosition,
    pub end: InputPosition,
}

impl InputSpan {
    pub fn from_endpoints(start: InputPosition, end: InputPosition) -> Self {
        InputSpan {
            start: start,
            end: end,
        }
    }

    pub fn extend(self, other: InputSpan) -> Self {
        InputSpan {
            start: self.start,
            end: other.end,
        }
    }

    pub fn start(&self) -> &InputPosition {
        &self.start
    }

    pub fn end(&self) -> &InputPosition {
        &self.end
    }
}
