use super::manipulation::*;
use super::super::IOByte;
use std::char;

#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum LinebreakCheckResult {
    NeitherByteIsLinebreak,
    FirstByteIsLinebreak,
    SecondByteIsLinebreak,
    EachByteIsLinebreak,
    BothBytesAreLinebreak,
}

pub fn is_ascii_char(byte: IOByte) -> bool {
    match byte {
        b'\x00'...b'\x7F' => true,
        _ => false,
    }
}

pub fn is_ascii_vchar(byte: IOByte) -> bool {
    match byte {
        b'!'...b'~' => true,
        _ => false,
    }
}

pub fn is_ascii_whitespace_char(byte: IOByte) -> bool {
    byte_to_char(byte).map(char::is_whitespace).unwrap_or(false)
}

pub fn is_ascii_terminal_punctuation_char(byte: IOByte) -> bool {
    match byte {
        b'!' | b',' | b'.' | b':' | b';' | b'?' => true,
        _ => false,
    }
}

pub fn is_ascii_open_close_punctuation_char(byte: IOByte) -> bool {
    match byte {
        b'(' | b')' | b'[' | b']' | b'{' | b'}' => true,
        _ => false,
    }
}

pub fn is_linebreak_byte(byte: IOByte) -> bool {
    match byte {
        b'\n' | b'\r' => true,
        _ => false,
    }
}

pub fn is_atom_byte(byte: IOByte) -> bool {
    match byte {
        b if !is_ascii_vchar(b) => false,
        b if is_ascii_terminal_punctuation_char(b) => false,
        b if is_ascii_open_close_punctuation_char(b) => false,
        b'#' | b'$' | b'\'' | b'`' => false,
        _ => true,
    }
}

pub fn linebreak_check(byte1: IOByte, byte2: IOByte) -> LinebreakCheckResult {
    // Accept any of {LF, CR, CR+LF, LF+CR} as a line-break.
    match (
        is_linebreak_byte(byte1),
        is_linebreak_byte(byte2),
        byte1 == byte2,
    ) {
        (false, false, false) => {
            // There's no line-break here.
            LinebreakCheckResult::NeitherByteIsLinebreak
        }
        (true, false, false) => {
            // The first byte is a line break or part of one, but the second is not.
            LinebreakCheckResult::FirstByteIsLinebreak
        }
        (false, true, false) => {
            // The second byte is a line break or part of one, but the first is not.
            LinebreakCheckResult::SecondByteIsLinebreak
        }
        (false, false, true) => {
            // This is also not a line-break.
            LinebreakCheckResult::NeitherByteIsLinebreak
        }
        (true, true, false) => {
            // This is a two-byte line-break.
            LinebreakCheckResult::BothBytesAreLinebreak
        }
        (true, false, true) => {
            // This should be impossible, because a byte that is a line-break byte and a byte that
            // is not should not compare equal.
            unreachable!()
        }
        (false, true, true) => {
            // This should also be impossible, for the same reason. This case has a separate
            // `unreachable!` declaration from the above one in case it might at some point become
            // useful to distinguish them for debugging.
            unreachable!()
        }
        (true, true, true) => {
            // Each byte is a line-break or part of one, but they are not a single two-byte
            // line-break.
            LinebreakCheckResult::EachByteIsLinebreak
        }
    }
}
