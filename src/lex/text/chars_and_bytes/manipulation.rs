use super::classification::*;
use super::super::IOByte;
use std::char;

pub fn byte_to_char(byte: IOByte) -> Option<char> {
    match is_ascii_char(byte) {
        true => char::from_u32(byte as u32),
        false => None,
    }
}

pub fn dbg_fmt_byte(byte: IOByte) -> String {
    format!(
        "'{}'",
        char::from_u32(byte as u32)
            .map(|c| c.escape_default().collect())
            .unwrap_or(format!("\\u{{{:X}}}", byte))
    )
}
