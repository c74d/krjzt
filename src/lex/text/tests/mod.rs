use super::*;
use super::phases::bytes::InputBytes;
use super::phases::last::InputToken;
use super::phases::last::InputTokens;
use super::phases::last::Token;
use super::super::InputResourceDescriptor;
use std::result;
use util::assert_all_eq;

type PosPair = (usize, usize);
type TokPosTriple = (PosPair, PosPair, Token);

fn atom(s: &str) -> Token {
    Token::Atom(s.into())
}

fn pos_pair(InputPosition { line, column, .. }: InputPosition) -> PosPair {
    (line, column)
}

fn tokpos_triple(
    InputToken {
        token,
        span: InputSpan { start, end },
    }: InputToken,
) -> TokPosTriple {
    (pos_pair(start), pos_pair(end), token)
}

fn lex_str(input: &str) -> InputTokens<&[u8]> {
    let input_bytes_iter = io::Read::bytes(input.as_bytes());
    let ird = InputResourceDescriptor::default();
    lex(input_bytes_iter, ird)
}

fn lex_check(input: &str, check: Vec<TokPosTriple>) {
    assert_all_eq(lex_str(input).map(|r| r.map(tokpos_triple).unwrap()), check)
}

#[test]
fn linebreak_check_1() {
    assert_eq!(
        linebreak_check(b'\0', b'\0'),
        LinebreakCheckResult::NeitherByteIsLinebreak
    );

    assert_eq!(
        linebreak_check(b'\n', b'\0'),
        LinebreakCheckResult::FirstByteIsLinebreak
    );
    assert_eq!(
        linebreak_check(b'\r', b'\0'),
        LinebreakCheckResult::FirstByteIsLinebreak
    );

    assert_eq!(
        linebreak_check(b'\0', b'\n'),
        LinebreakCheckResult::SecondByteIsLinebreak
    );
    assert_eq!(
        linebreak_check(b'\0', b'\r'),
        LinebreakCheckResult::SecondByteIsLinebreak
    );

    assert_eq!(
        linebreak_check(b'\n', b'\n'),
        LinebreakCheckResult::EachByteIsLinebreak
    );
    assert_eq!(
        linebreak_check(b'\r', b'\r'),
        LinebreakCheckResult::EachByteIsLinebreak
    );

    assert_eq!(
        linebreak_check(b'\n', b'\r'),
        LinebreakCheckResult::BothBytesAreLinebreak
    );
    assert_eq!(
        linebreak_check(b'\r', b'\n'),
        LinebreakCheckResult::BothBytesAreLinebreak
    );
}

fn str_bytes(input: &str) -> InputBytes<&[u8]> {
    InputBytes::new(
        io::Read::bytes(input.as_bytes()),
        InputResourceDescriptor::default(),
    )
}

type ByteStepQuad = (usize, usize, result::Result<Option<char>, String>, Option<Option<char>>);

fn bytes_iter_check(input: &str, check: Vec<ByteStepQuad>) {
    println!("");
    let mut i = 0;
    let iter = str_bytes(input).map(|r| {
        let step = r.unwrap();
        assert_eq!(step.pos.byte_pos(), i);
        i += 1;
        let r = match step.byte {
            Ok(b) => Ok(byte_to_char(b)),
            Err(e) => Err(format!("{:?}", e)),
        };
        (
            step.pos.line(),
            step.pos.column(),
            r,
            step.prev_byte.map(byte_to_char),
        )
    });
    assert_all_eq(iter, check)
}

#[test]
fn bytes_iter_1() {
    bytes_iter_check(
        "abc",
        vec![
            (1, 01, Ok(Some('a')), None),
            (1, 02, Ok(Some('b')), Some(Some('a'))),
            (1, 03, Ok(Some('c')), Some(Some('b'))),
        ],
    )
}

#[test]
fn bytes_iter_2() {
    bytes_iter_check(
        "abc\nxyz",
        vec![
            (1, 01, Ok(Some('a')), None),
            (1, 02, Ok(Some('b')), Some(Some('a'))),
            (1, 03, Ok(Some('c')), Some(Some('b'))),
            (1, 04, Ok(Some('\n')), Some(Some('c'))),
            (2, 01, Ok(Some('x')), Some(Some('\n'))),
            (2, 02, Ok(Some('y')), Some(Some('x'))),
            (2, 03, Ok(Some('z')), Some(Some('y'))),
        ],
    )
}

#[test]
fn bytes_iter_3() {
    bytes_iter_check(
        "abc\n\nxyz",
        vec![
            (1, 01, Ok(Some('a')), None),
            (1, 02, Ok(Some('b')), Some(Some('a'))),
            (1, 03, Ok(Some('c')), Some(Some('b'))),
            (1, 04, Ok(Some('\n')), Some(Some('c'))),
            (2, 01, Ok(Some('\n')), Some(Some('\n'))),
            (3, 01, Ok(Some('x')), Some(Some('\n'))),
            (3, 02, Ok(Some('y')), Some(Some('x'))),
            (3, 03, Ok(Some('z')), Some(Some('y'))),
        ],
    )
}

#[test]
fn bytes_iter_4() {
    bytes_iter_check(
        "abc\r\nxyz",
        vec![
            (1, 01, Ok(Some('a')), None),
            (1, 02, Ok(Some('b')), Some(Some('a'))),
            (1, 03, Ok(Some('c')), Some(Some('b'))),
            (1, 04, Ok(Some('\r')), Some(Some('c'))),
            (1, 04, Ok(Some('\n')), Some(Some('\r'))),
            (2, 01, Ok(Some('x')), Some(Some('\n'))),
            (2, 02, Ok(Some('y')), Some(Some('x'))),
            (2, 03, Ok(Some('z')), Some(Some('y'))),
        ],
    )
}

#[test]
fn lex_simple_colon() {
    lex_check(":", vec![((1, 01), (1, 01), Token::Colon)]);
}

#[test]
fn lex_simple_paren_start() {
    lex_check("(", vec![((1, 01), (1, 01), Token::ParenthesisStart)]);
}

#[test]
fn lex_simple_paren_end() {
    lex_check(")", vec![((1, 01), (1, 01), Token::ParenthesisEnd)]);
}

#[test]
fn lex_simple_bracket_square_start() {
    lex_check("[", vec![((1, 01), (1, 01), Token::BracketSquareStart)]);
}

#[test]
fn lex_simple_bracket_square_end() {
    lex_check("]", vec![((1, 01), (1, 01), Token::BracketSquareEnd)]);
}

#[test]
fn lex_simple_bracket_curly_start() {
    lex_check("{", vec![((1, 01), (1, 01), Token::BracketCurlyStart)]);
}

#[test]
fn lex_simple_bracket_curly_end() {
    lex_check("}", vec![((1, 01), (1, 01), Token::BracketCurlyEnd)]);
}

#[test]
fn lex_simple_atom_1() {
    lex_check("a", vec![((1, 01), (1, 01), atom("a"))]);
}

#[test]
fn lex_simple_atom_2() {
    lex_check("abc", vec![((1, 01), (1, 03), atom("abc"))]);
}

#[test]
fn lex_simple_string_literal_1() {
    lex_check("\"a\"", vec![((1, 01), (1, 03), atom("\"a\""))]);
}

#[test]
fn lex_simple_string_literal_2() {
    lex_check("\"abc\"", vec![((1, 01), (1, 05), atom("\"abc\""))]);
}

#[test]
fn lex_simple_linebreak_lf() {
    lex_check("\n", vec![((1, 01), (1, 01), Token::Linebreak)]);
}

#[test]
fn lex_simple_linebreak_cr() {
    lex_check("\r", vec![((1, 01), (1, 01), Token::Linebreak)]);
}

#[test]
fn lex_simple_linebreak_crlf() {
    lex_check("\r\n", vec![((1, 01), (1, 01), Token::Linebreak)]);
}

#[test]
fn lex_simple_linebreak_lfcr() {
    lex_check("\n\r", vec![((1, 01), (1, 01), Token::Linebreak)]);
}

#[test]
fn lex_single_line_1() {
    lex_check(
        "(:[ {:}] )",
        vec![
            ((1, 01), (1, 01), Token::ParenthesisStart),
            ((1, 02), (1, 02), Token::Colon),
            ((1, 03), (1, 03), Token::BracketSquareStart),
            ((1, 04), (1, 04), Token::Space),
            ((1, 05), (1, 05), Token::BracketCurlyStart),
            ((1, 06), (1, 06), Token::Colon),
            ((1, 07), (1, 07), Token::BracketCurlyEnd),
            ((1, 08), (1, 08), Token::BracketSquareEnd),
            ((1, 09), (1, 09), Token::Space),
            ((1, 10), (1, 10), Token::ParenthesisEnd),
        ],
    );
}

#[test]
fn lex_multi_line_1() {
    lex_check(
        "[[\n:\n\r\t{\n\t\t)\r\n\t(}\r\t:",
        vec![
            ((1, 01), (1, 01), Token::BracketSquareStart),
            ((1, 02), (1, 02), Token::BracketSquareStart),
            ((1, 03), (2, 01), Token::LinebreakColon),
            ((2, 02), (2, 02), Token::Linebreak),
            ((3, 01), (3, 01), Token::Indent),
            ((3, 02), (3, 02), Token::BracketCurlyStart),
            ((3, 03), (3, 03), Token::Linebreak),
            ((4, 01), (4, 02), Token::Indent),
            ((4, 03), (4, 03), Token::ParenthesisEnd),
            ((4, 04), (4, 04), Token::Linebreak),
            ((5, 01), (5, 01), Token::Dedent),
            ((5, 02), (5, 02), Token::ParenthesisStart),
            ((5, 03), (5, 03), Token::BracketCurlyEnd),
            ((5, 04), (6, 02), Token::LinebreakColon),
        ],
    );
}

#[test]
fn lex_multi_line_2() {
    lex_check(
        include_str!("src-snippets/multi-line-2.krjzt"),
        vec![
            ((1, 01), (1, 01), Token::Linebreak),
            ((2, 01), (2, 02), atom("fn")),
            ((2, 03), (2, 03), Token::Space),
            ((2, 04), (2, 04), atom("f")),
            ((2, 05), (2, 05), Token::Space),
            ((2, 06), (2, 06), Token::ParenthesisStart),
            ((2, 07), (2, 07), atom("x")),
            ((2, 08), (2, 08), Token::Colon),
            ((2, 09), (2, 09), Token::Space),
            ((2, 10), (2, 23), atom("1st-param-type")),
            ((2, 24), (2, 24), Token::Semicolon),
            ((2, 25), (2, 25), Token::Space),
            ((2, 26), (2, 36), atom("other-param")),
            ((2, 37), (2, 37), Token::Colon),
            ((2, 38), (2, 38), Token::Space),
            ((2, 39), (2, 39), atom("T")),
            ((2, 40), (2, 40), Token::ParenthesisEnd),
            ((2, 41), (2, 41), Token::Colon),
            ((2, 42), (2, 42), Token::Space),
            ((2, 43), (2, 43), atom("T")),
            ((2, 44), (2, 44), Token::Colon),
            ((2, 45), (2, 45), Token::Linebreak),
            ((3, 01), (3, 01), Token::Indent),
            ((3, 02), (3, 03), atom("if")),
            ((3, 04), (3, 04), Token::Space),
            ((3, 05), (3, 05), atom("x")),
            ((3, 06), (3, 06), Token::Space),
            ((3, 07), (3, 09), atom(">|>")),
            ((3, 10), (3, 10), Token::Space),
            ((3, 11), (3, 21), atom("other-param")),
            ((3, 22), (3, 22), Token::Colon),
            ((3, 23), (3, 23), Token::Linebreak),
            ((4, 01), (4, 02), Token::Indent),
            ((4, 03), (4, 13), atom("other-param")),
            ((4, 14), (4, 14), Token::Linebreak),
            ((5, 01), (5, 01), Token::Dedent),
            ((5, 02), (5, 05), atom("else")),
            ((5, 06), (5, 06), Token::Colon),
            ((5, 07), (5, 07), Token::Linebreak),
            ((6, 01), (6, 02), Token::Indent),
            ((6, 03), (6, 03), atom("x")),
            ((6, 04), (6, 04), Token::Space),
            ((6, 05), (6, 05), atom("+")),
            ((6, 06), (6, 06), Token::Space),
            ((6, 07), (6, 21), atom("123_456_789_abc")),
            ((6, 22), (6, 22), Token::Linebreak),
            ((7, 01), (7, 01), Token::Dedent),
            ((7, 01), (7, 01), Token::Dedent),
            ((7, 01), (7, 01), Token::Linebreak),
        ],
    );
}

#[test]
fn lex_multi_line_3() {
    lex_check(
        include_str!("src-snippets/multi-line-3.krjzt"),
        vec![
            ((1, 01), (1, 01), Token::Linebreak),
            ((2, 01), (2, 02), atom("fn")),
            ((2, 03), (2, 03), Token::Space),
            ((2, 04), (2, 04), atom("f")),
            ((2, 05), (2, 05), Token::Space),
            ((2, 06), (2, 06), Token::ParenthesisStart),
            ((2, 07), (2, 07), atom("x")),
            ((2, 08), (2, 08), Token::Colon),
            ((2, 09), (2, 09), Token::Space),
            ((2, 10), (2, 10), atom("T")),
            ((2, 11), (2, 11), Token::Semicolon),
            ((2, 12), (2, 12), Token::Space),
            ((2, 13), (2, 13), atom("y")),
            ((2, 14), (2, 14), Token::Colon),
            ((2, 15), (2, 15), Token::Space),
            ((2, 16), (2, 16), atom("U")),
            ((2, 17), (2, 17), Token::ParenthesisEnd),
            ((2, 18), (2, 18), Token::Colon),
            ((2, 19), (2, 19), Token::Space),
            ((2, 20), (2, 20), atom("T")),
            ((2, 21), (2, 21), Token::Colon),
            ((2, 22), (2, 22), Token::Linebreak),
            ((3, 01), (3, 01), Token::Indent),
            ((3, 02), (3, 03), atom("if")),
            ((3, 04), (3, 04), Token::Space),
            ((3, 05), (3, 05), atom("x")),
            ((3, 06), (3, 06), Token::Space),
            ((3, 07), (3, 08), atom("m1")),
            ((3, 09), (3, 09), Token::Space),
            ((3, 10), (3, 10), atom("y")),
            ((3, 11), (3, 11), Token::Colon),
            ((3, 12), (3, 12), Token::Linebreak),
            ((4, 01), (4, 02), Token::Indent),
            ((4, 03), (4, 03), atom("x")),
            ((4, 04), (4, 04), Token::Space),
            ((4, 05), (4, 06), atom("m2")),
            ((4, 07), (4, 07), Token::Space),
            ((4, 08), (4, 08), atom("y")),
            ((4, 09), (4, 09), Token::Linebreak),
            ((5, 01), (5, 01), Token::Dedent),
            ((5, 01), (5, 01), Token::Dedent),
            ((5, 01), (5, 01), Token::Linebreak),
            ((6, 01), (6, 05), atom("const")),
            ((6, 06), (6, 06), Token::Space),
            ((6, 07), (6, 07), atom("n")),
            ((6, 08), (6, 08), Token::Colon),
            ((6, 09), (6, 09), Token::Space),
            ((6, 10), (6, 10), atom("8")),
            ((6, 11), (6, 11), Token::Linebreak),
            ((7, 01), (7, 01), Token::Linebreak),
            ((8, 01), (8, 05), atom("const")),
            ((8, 06), (8, 06), Token::Space),
            ((8, 07), (8, 08), atom("s1")),
            ((8, 09), (8, 09), Token::Colon),
            ((8, 10), (8, 10), Token::Space),
            ((8, 11), (8, 11), atom("g")),
            ((8, 12), (8, 12), Token::Space),
            ((8, 13), (8, 21), atom("\"abc xyz\"")),
            ((8, 22), (8, 22), Token::Space),
            ((8, 23), (8, 23), atom("m")),
            ((8, 24), (8, 24), Token::Linebreak),
            ((9, 01), (9, 01), Token::Linebreak),
        ],
    );
}
