use super::super::ErrorKind;
use super::super::IOByte;
use super::super::IOByteResult;
use super::super::InputPosition;
use super::super::LinebreakCheckResult;
use super::super::Result;
use super::super::dbg_fmt_byte;
use super::super::linebreak_check;
use lex::InputResourceDescriptor;
use std::io;
use std::iter;

pub struct InputBytes<R>
where
    R: io::Read,
{
    raw_bytes_iter: iter::Peekable<io::Bytes<R>>,
    input_resrc: InputResourceDescriptor,
    /// Zero-based offset into the bytes of the input resource.
    byte_pos: usize,
    /// One-based line number.
    line: usize,
    /// One-based column number.
    column: usize,
    prev_byte: Option<IOByte>,
}

impl<R> InputBytes<R>
where
    R: io::Read,
{
    pub fn new(raw_bytes_iter: io::Bytes<R>, input_resrc: InputResourceDescriptor) -> Self {
        InputBytes {
            raw_bytes_iter: raw_bytes_iter.peekable(),
            input_resrc: input_resrc,
            byte_pos: 0,
            line: 1,
            column: 1,
            prev_byte: None,
        }
    }

    fn pos(&self) -> InputPosition {
        InputPosition::new(
            self.input_resrc.clone(),
            self.byte_pos,
            self.line,
            self.column,
        )
    }

    fn increment_byte_count(&mut self) -> Result<()> {
        self.byte_pos = self.byte_pos.checked_add(1).ok_or_else(|| {
            ErrorKind::ByteCountOverflow(self.pos())
        })?;
        Ok(())
    }

    fn increment_column_count(&mut self) -> Result<()> {
        self.column = self.column.checked_add(1).ok_or_else(|| {
            ErrorKind::ColumnCountOverflow(self.pos())
        })?;
        Ok(())
    }

    fn increment_line_count(&mut self) -> Result<()> {
        self.line = self.line.checked_add(1).ok_or_else(|| {
            ErrorKind::LineCountOverflow(self.pos())
        })?;
        self.column = 1;
        Ok(())
    }

    fn dbg_gah(&self, byte: IOByte, linebreak_status: LinebreakCheckResult) {
        if !cfg!(test) {
            return;
        }

        println!(
            "{bpos:3} | {line:02}:{column:02} {byte:>4} | prev {prev:>4} | {lbs:?}",
            bpos = self.byte_pos,
            line = self.line,
            column = self.column,
            byte = dbg_fmt_byte(byte),
            prev = self.prev_byte.map(dbg_fmt_byte).unwrap_or("- ".to_string()),
            lbs = linebreak_status
        );
    }
}

impl<R> Iterator for InputBytes<R>
where
    R: io::Read,
{
    type Item = Result<InputBytesStep<R>>;

    fn next(&mut self) -> Option<Result<InputBytesStep<R>>> {
        let byte_result = match self.raw_bytes_iter.next() {
            Some(b) => b,
            None => return None,
        };

        let prev_byte = self.prev_byte;

        let next_byte = match self.raw_bytes_iter.peek() {
            Some(&Ok(b)) => b,
            _ => b'\0',
        };

        let result = InputBytesStep {
            byte: byte_result,
            pos: self.pos(),
            prev_byte: prev_byte,
        };

        if let Ok(byte) = result.byte {
            let linebreak_status = linebreak_check(byte, next_byte);

            self.dbg_gah(byte, linebreak_status);

            let incr_result = match linebreak_status {
                LinebreakCheckResult::NeitherByteIsLinebreak => {
                    // We're not on a line-break. Additionally, the next byte is not a line-break
                    // nor part of one.
                    self.increment_column_count()
                }
                LinebreakCheckResult::FirstByteIsLinebreak => {
                    // We're on a single-byte line-break or the second byte of a two-byte
                    // line-break.
                    self.increment_line_count()
                }
                LinebreakCheckResult::SecondByteIsLinebreak => {
                    // We're not on a line-break. However, the next byte is a line-break or part of
                    // one.
                    self.increment_column_count()
                }
                LinebreakCheckResult::EachByteIsLinebreak => {
                    // We're on either a single-byte line-break or the second byte of a two-byte
                    // line-break. Additionally, the next byte is either a single-byte line-break
                    // or the first byte of a two-byte line-break.
                    self.increment_line_count()
                }
                LinebreakCheckResult::BothBytesAreLinebreak => {
                    // We're on the first byte of a two-byte line-break, which should be processed
                    // on the next iteration (at which point we should reach the second byte of the
                    // line-break).
                    Ok(())
                }
            };

            if let Err(err) = incr_result {
                return Some(Err(err));
            }

            self.prev_byte = Some(byte);
        }

        if let Err(err) = self.increment_byte_count() {
            return Some(Err(err));
        }

        Some(Ok(result))
    }
}

#[derive(Debug)]
pub struct InputBytesStep<R>
where
    R: io::Read,
{
    pub byte: IOByteResult<R>,
    pub pos: InputPosition,
    pub prev_byte: Option<IOByte>,
}
