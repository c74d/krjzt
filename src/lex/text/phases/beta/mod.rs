pub use self::input_token::*;
use super::alpha;
use super::super::Result;
use std::i8;
use std::io;
use std::usize;
use string_cache::DefaultAtom as AtomImpl;
use util::UnsignedAbs;

mod input_token;

#[derive(Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum Token {
    Atom(AtomImpl),
    Space,
    Linebreak,
    ExclamationMark,
    Hash,
    DollarSymbol,
    ParenthesisStart,
    ParenthesisEnd,
    Comma,
    Dot,
    Colon,
    Semicolon,
    QuestionMark,
    BracketSquareStart,
    BracketSquareEnd,
    BracketCurlyStart,
    BracketCurlyEnd,
    Indent,
    Dedent,
}

pub struct InputTokens<R>
where
    R: io::Read,
{
    input_iter: alpha::InputTokens<R>,
    repeat_token: Option<InputToken>,
    repeat_qty: usize,
}

impl<R> InputTokens<R>
where
    R: io::Read,
{
    pub fn from_alpha(input_iter: alpha::InputTokens<R>) -> Self {
        InputTokens {
            input_iter: input_iter,
            repeat_token: None,
            repeat_qty: 0,
        }
    }
}

impl<R> Iterator for InputTokens<R>
where
    R: io::Read,
{
    type Item = Result<InputToken>;

    fn next(&mut self) -> Option<Result<InputToken>> {
        match (self.repeat_token.clone(), self.repeat_qty) {
            (Some(t), 2...usize::MAX) => {
                self.repeat_qty -= 1;
                return Some(Ok(t));
            }
            (Some(t), 1) => {
                self.repeat_token = None;
                self.repeat_qty = 0;
                return Some(Ok(t));
            }
            (None, 0) => {}
            _ => unreachable!(),
        }

        let alpha::InputToken { value, span } = match self.input_iter.next() {
            Some(Ok(alpha_tok)) => alpha_tok,
            Some(Err(err)) => return Some(Err(err)),
            None => return None,
        };

        use self::alpha::Token as AT;

        let output_token = match value {
            AT::Atom(s) => Token::Atom(s),
            AT::Space => Token::Space,
            AT::Linebreak(indent_chg, indent_span) => {
                self.repeat_token = match indent_chg {
                    i8::MIN...-1 => Some(InputToken::new(Token::Dedent, indent_span)),
                    0 => None,
                    1...i8::MAX => Some(InputToken::new(Token::Indent, indent_span)),
                    _ => unreachable!(),
                };
                self.repeat_qty = indent_chg.uabs().into();
                Token::Linebreak
            }
            AT::ExclamationMark => Token::ExclamationMark,
            AT::Hash => Token::Hash,
            AT::DollarSymbol => Token::DollarSymbol,
            AT::ParenthesisStart => Token::ParenthesisStart,
            AT::ParenthesisEnd => Token::ParenthesisEnd,
            AT::Comma => Token::Comma,
            AT::Dot => Token::Dot,
            AT::Colon => Token::Colon,
            AT::Semicolon => Token::Semicolon,
            AT::QuestionMark => Token::QuestionMark,
            AT::BracketSquareStart => Token::BracketSquareStart,
            AT::BracketSquareEnd => Token::BracketSquareEnd,
            AT::BracketCurlyStart => Token::BracketCurlyStart,
            AT::BracketCurlyEnd => Token::BracketCurlyEnd,
        };

        Some(Ok(InputToken {
            value: output_token,
            span: span,
        }))
    }
}
