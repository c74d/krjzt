use super::beta;
use super::super::InputSpan;
use super::super::Result;
use std::io;
use std::iter;
use string_cache::DefaultAtom as AtomImpl;

#[derive(Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum Token {
    Atom(AtomImpl),
    Space,
    Linebreak,
    ExclamationMark,
    Hash,
    DollarSymbol,
    ParenthesisStart,
    ParenthesisEnd,
    Comma,
    Dot,
    Colon,
    Semicolon,
    QuestionMark,
    BracketSquareStart,
    BracketSquareEnd,
    BracketCurlyStart,
    BracketCurlyEnd,
    Indent,
    Dedent,
    LinebreakColon,
}

pub struct InputToken {
    pub token: Token,
    pub span: InputSpan,
}

pub struct InputTokens<R>
where
    R: io::Read,
{
    input_iter: iter::Peekable<beta::InputTokens<R>>,
}

impl<R> InputTokens<R>
where
    R: io::Read,
{
    pub fn from_beta(input_iter: beta::InputTokens<R>) -> Self {
        InputTokens { input_iter: input_iter.peekable() }
    }

    fn assured_next(&mut self) -> (beta::Token, InputSpan) {
        match self.input_iter.next() {
            Some(Ok(beta::InputToken { value, span })) => (value, span),
            Some(Err(_)) => unreachable!(),
            None => unreachable!(),
        }
    }
}

impl<R> Iterator for InputTokens<R>
where
    R: io::Read,
{
    type Item = Result<InputToken>;

    fn next(&mut self) -> Option<Result<InputToken>> {
        let beta::InputToken { value, span } = match self.input_iter.next() {
            Some(Ok(beta_tok)) => beta_tok,
            Some(Err(err)) => return Some(Err(err)),
            None => return None,
        };

        let peek = match self.input_iter.peek() {
            Some(&Ok(beta::InputToken { ref value, .. })) => Some(value.clone()),
            _ => None,
        };

        use self::beta::Token as BT;

        let output_token = match value {
            BT::Atom(s) => Token::Atom(s),
            BT::Space => Token::Space,
            BT::Linebreak => {
                match peek {
                    Some(BT::Colon) => {
                        let (_, span_end) = self.assured_next();
                        return mk_tok(Token::LinebreakColon, span.extend(span_end));
                    }
                    _ => Token::Linebreak,
                }
            }
            BT::ExclamationMark => Token::ExclamationMark,
            BT::Hash => Token::Hash,
            BT::DollarSymbol => Token::DollarSymbol,
            BT::ParenthesisStart => Token::ParenthesisStart,
            BT::ParenthesisEnd => Token::ParenthesisEnd,
            BT::Comma => Token::Comma,
            BT::Dot => Token::Dot,
            BT::Colon => Token::Colon,
            BT::Semicolon => Token::Semicolon,
            BT::QuestionMark => Token::QuestionMark,
            BT::BracketSquareStart => Token::BracketSquareStart,
            BT::BracketSquareEnd => Token::BracketSquareEnd,
            BT::BracketCurlyStart => Token::BracketCurlyStart,
            BT::BracketCurlyEnd => Token::BracketCurlyEnd,
            BT::Indent => Token::Indent,
            BT::Dedent => Token::Dedent,
        };

        mk_tok(output_token, span)
    }
}

fn mk_tok(token: Token, span: InputSpan) -> Option<Result<InputToken>> {
    Some(Ok(InputToken {
        token: token,
        span: span,
    }))
}
