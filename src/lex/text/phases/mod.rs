
/// The `alpha` phase of lexing takes bytes from the `bytes` phase and produces tokens.
pub mod alpha;

/// The `beta` phase of lexing processes the tokens from the `alpha` phase to synthesize `Indent`
/// and `Dedent` tokens.
pub mod beta;

/// The `bytes` phase of lexing attaches metadata to the bytes of the textual input.
pub mod bytes;

/// The `gamma` phase of lexing processes the tokens from the `beta` phase to combine certain
/// important sequences of tokens into single tokens.
pub mod gamma;

/// An alias for the current final phase of lexing, that should be updated whenever another phase
/// is added.
pub use self::gamma as last;
