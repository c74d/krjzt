use super::Token;
use lex::text::InputSpan;

#[derive(Clone, Debug, PartialEq)]
pub struct InputToken {
    pub value: Token,
    pub span: InputSpan,
}

impl InputToken {
    pub fn new(value: Token, span: InputSpan) -> Self {
        InputToken {
            value: value,
            span: span,
        }
    }
}
