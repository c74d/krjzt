pub use self::input_token::*;
use super::bytes::InputBytes;
use super::bytes::InputBytesStep;
use super::super::ErrorKind;
use super::super::IOByte;
use super::super::IndentChange;
use super::super::IndentLevel;
use super::super::InputPosition;
use super::super::InputSpan;
use super::super::LinebreakCheckResult;
use super::super::Result;
use super::super::chars_and_bytes::*;
use smallvec::SmallVec;
use std::borrow::Cow;
use std::i8;
use std::io;
use std::iter;
use std::str;
use std::string::FromUtf8Error;
use string_cache::DefaultAtom as AtomImpl;
use string_cache_shared;

mod input_token;

// `string_cache` will store an atom at most `MAX_INLINE_LEN` bytes in length without allocation,
// as I understand it. Store atoms of at most `MAX_INLINE_LEN` bytes on the stack from the start;
// store longer atoms on the heap.
type AtomBuf = SmallVec<[u8; string_cache_shared::MAX_INLINE_LEN]>;

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Token {
    Atom(AtomImpl),
    Space,
    /// The `IndentChange` field records the difference between the indentation level of the line
    /// preceding the line-break and the indentation level of the line following the line-break.
    /// The `InputSpan` field records the span of the indentation following (and not including) the
    /// line-break.
    Linebreak(IndentChange, InputSpan),
    ExclamationMark,
    Hash,
    DollarSymbol,
    ParenthesisStart,
    ParenthesisEnd,
    Comma,
    Dot,
    Colon,
    Semicolon,
    QuestionMark,
    BracketSquareStart,
    BracketSquareEnd,
    BracketCurlyStart,
    BracketCurlyEnd,
}

/// An iterator over tokens, this type contains the main part of the text lexer implementation.
pub struct InputTokens<R>
where
    R: io::Read,
{
    input_bytes_iter: iter::Peekable<InputBytes<R>>,
    indentation_level: IndentLevel,
}

impl<R> InputTokens<R>
where
    R: io::Read,
{
    pub fn from_bytes(input_bytes_iter: InputBytes<R>) -> Self {
        InputTokens {
            input_bytes_iter: input_bytes_iter.peekable(),
            indentation_level: 0,
        }
    }

    fn peek_byte_or_nul(&mut self) -> IOByte {
        match self.input_bytes_iter.peek() {
            Some(&Ok(InputBytesStep { byte: Ok(b), .. })) => b,
            _ => b'\0',
        }
    }

    fn discard_byte(&mut self) {
        self.input_bytes_iter.next();
    }

    fn lex_linebreak(&mut self, start_byte: IOByte) -> bool {
        // If we're on the first byte of a line-break, return `true` and leave
        // `self.input_bytes_iter` such that the next call to `next` on it will return the first
        // byte after the line-break; otherwise, return `false`. This should accept {LF, CR, CR+LF,
        // LF+CR} as line-breaks.
        match linebreak_check(start_byte, self.peek_byte_or_nul()) {
            LinebreakCheckResult::NeitherByteIsLinebreak => {
                // We are not on a line-break, and the next byte is neither a single-byte
                // line-break nor the first byte of a two-byte line-break.
                false
            }
            LinebreakCheckResult::FirstByteIsLinebreak => {
                // We are on a one-byte line-break.
                true
            }
            LinebreakCheckResult::SecondByteIsLinebreak => {
                // We are not on a line-break. The next byte is either a single-byte line-break or
                // the first byte of a two-byte line-break, but we'll handle that when we get
                // there.
                false
            }
            LinebreakCheckResult::EachByteIsLinebreak => {
                // We are on a one-byte line-break. The next byte is also a line-break or part of
                // one, but, again, we'll handle that when we get there.
                true
            }
            LinebreakCheckResult::BothBytesAreLinebreak => {
                // We are on a two-byte line-break. Consume the second byte.
                self.discard_byte();
                true
            }
        }
    }

    fn lex_indentation(
        &mut self,
        linebreak_pos: &InputPosition,
    ) -> Result<(IndentLevel, InputSpan)> {
        let span_start = match self.input_bytes_iter.peek() {
            Some(&Ok(InputBytesStep { ref pos, .. })) => pos.clone(),
            _ => linebreak_pos.clone(),
        };
        let mut span_end = span_start.clone();
        let mut lvl: IndentLevel = 0;

        // TODO: Once non-lexical lifetimes hit stable, try using `while let` here.
        loop {
            if let Some(&Ok(InputBytesStep {
                                byte: Ok(b'\t'),
                                ref pos,
                                ..
                            })) = self.input_bytes_iter.peek()
            {
                lvl = match lvl.checked_add(1) {
                    Some(n @ 0...i8::MAX) => n,
                    _ => bail!(ErrorKind::ExcessiveIndentation(pos.clone())),
                };
                span_end = pos.clone();
            } else {
                break;
            }
            self.discard_byte();
        }

        Ok((lvl, InputSpan::from_endpoints(span_start, span_end)))
    }

    fn lex_start_of_line(
        &mut self,
        pos: InputPosition,
        start_byte: IOByte,
    ) -> Option<Result<InputToken>> {
        // Note that `lex_linebreak` is effectful -- it moves the lexer past the line-break.
        let on_linebreak = self.lex_linebreak(start_byte);
        debug_assert!(
            on_linebreak,
            "`lex_start_of_line` has apparently been called when the lexer is not on a line-break \
             (start_byte: {start_byte}, self.peek_byte_or_nul(): {peek})",
            start_byte = dbg_fmt_byte(start_byte),
            peek = dbg_fmt_byte(self.peek_byte_or_nul())
        );

        let (new_indentation_level, indentation_span) = match self.lex_indentation(&pos) {
            Ok(i) => i,
            Err(err) => return Some(Err(err)),
        };
        let indentation_change = (new_indentation_level - self.indentation_level) as IndentChange;
        self.indentation_level = new_indentation_level;

        mk_token(pos, Token::Linebreak(indentation_change, indentation_span))
    }

    fn lex_atom(
        &mut self,
        span_start: InputPosition,
        start_byte: IOByte,
    ) -> Option<Result<InputToken>> {
        #[derive(Debug, PartialEq)]
        enum State {
            Normal,
            StringLiteral { terminator: AtomBuf },
            Done,
        }

        let update_state =
            |current_state: State, buf: &[IOByte], next_byte: IOByte| match current_state {
                State::Normal => {
                    if !is_atom_byte(next_byte) {
                        return State::Done;
                    }

                    if next_byte == b'"' {
                        State::StringLiteral {
                            terminator: if buf == b"" {
                                AtomBuf::from_slice(b"\"")
                            } else {
                                panic!(
                                    "unimplemented: string literal type {:?}",
                                    str::from_utf8(buf)
                                )
                            },
                        }
                    } else {
                        State::Normal
                    }
                }
                State::StringLiteral { terminator } => {
                    if {
                        let (last, init) = terminator.split_last().expect(
                            "`terminator` mustn't be empty!",
                        );
                        *last == next_byte && buf.ends_with(init)
                    }
                    {
                        State::Normal
                    } else {
                        State::StringLiteral { terminator: terminator }
                    }
                }
                State::Done => unreachable!(),
            };

        let mut buf = AtomBuf::new();

        let mut state = update_state(State::Normal, buf.as_ref(), start_byte);

        // `state` being initialized to `Done` would mean that `start_byte` was not a valid atom
        // start byte.
        debug_assert_ne!(state, State::Done);

        buf.push(start_byte);

        let mut span_end = span_start.clone();

        // TODO: Once non-lexical lifetimes hit stable, try using `while let` here.
        loop {
            if let Some(&Ok(InputBytesStep {
                                byte: Ok(next_byte),
                                ref pos,
                                ..
                            })) = self.input_bytes_iter.peek()
            {
                state = update_state(state, buf.as_ref(), next_byte);

                if state == State::Done {
                    break;
                }

                buf.push(next_byte);

                span_end = pos.clone();
            } else {
                break;
            }
            self.discard_byte();
        }

        mk_atom(buf, span_start, span_end)
    }
}

impl<R> Iterator for InputTokens<R>
where
    R: io::Read,
{
    type Item = Result<InputToken>;

    fn next(&mut self) -> Option<Result<InputToken>> {
        let InputBytesStep {
            byte,
            pos,
            prev_byte,
        } = match self.input_bytes_iter.next() {
            Some(Ok(step)) => step,
            Some(Err(err)) => return Some(Err(err)),
            None => return None,
        };

        let input_byte = match byte {
            Ok(b) => b,
            Err(e) => return mk_io_err(e, pos),
        };

        let result = match input_byte {
            b'\t' => Some(Err(ErrorKind::StrayTabChar(pos).into())),
            b if is_linebreak_byte(b) => self.lex_start_of_line(pos, b),
            b' ' => {
                match prev_byte {
                    Some(b) if is_ascii_whitespace_char(b) => {
                        Some(Err(ErrorKind::StraySpaceChar(pos).into()))
                    }
                    _ => mk_token(pos, Token::Space),
                }
            }
            b'!' => mk_token(pos, Token::ExclamationMark),
            b'#' => mk_token(pos, Token::Hash),
            b'$' => mk_token(pos, Token::DollarSymbol),
            b'\'' => unimplemented!(),
            b'(' => mk_token(pos, Token::ParenthesisStart),
            b')' => mk_token(pos, Token::ParenthesisEnd),
            b',' => mk_token(pos, Token::Comma),
            b'.' => mk_token(pos, Token::Dot),
            b':' => mk_token(pos, Token::Colon),
            b';' => mk_token(pos, Token::Semicolon),
            b'?' => mk_token(pos, Token::QuestionMark),
            b'[' => mk_token(pos, Token::BracketSquareStart),
            b']' => mk_token(pos, Token::BracketSquareEnd),
            b'`' => unimplemented!(),
            b'{' => mk_token(pos, Token::BracketCurlyStart),
            b'}' => mk_token(pos, Token::BracketCurlyEnd),
            b if is_atom_byte(b) => self.lex_atom(pos, b),
            b if is_ascii_terminal_punctuation_char(b) => panic!("unimplemented: lexing {}", b),
            b if !is_ascii_char(b) => unimplemented!(),
            _ => unimplemented!(),
        };

        result
    }
}

fn mk_token(pos: InputPosition, value: Token) -> Option<Result<InputToken>> {
    Some(Ok(InputToken::new(
        value,
        InputSpan::from_endpoints(pos.clone(), pos.clone()),
    )))
}

fn mk_atom(
    mut buf: AtomBuf,
    span_start: InputPosition,
    span_end: InputPosition,
) -> Option<Result<InputToken>> {
    let mk_atom_final = |atom_string: Cow<str>, span: InputSpan| {
        Some(Ok(InputToken::new(Token::Atom(atom_string.into()), span)))
    };

    buf.shrink_to_fit();

    let span = InputSpan::from_endpoints(span_start, span_end);

    // As I understand it, turning a `&str` or `String` `into` a `string_cache` atom involves
    // first turning it into a `Cow<str>`, then calling `into_owned` on that `Cow`. (The
    // conversion to a `Cow` can be done automatically in most cases, but I opt to do it myself
    // here, for greater control.)

    // If the buffer is already on the heap, take it as a normal `Vec` and turn that into an
    // owning `String`, which should be able to be moved into being an atom, requiring no
    // cloning nor further allocation.
    if buf.spilled() {
        return match String::from_utf8(buf.into_vec()) {
            Ok(s) => mk_atom_final(Cow::Owned(s), span),
            Err(err) => mk_utf8_err(err, span),
        };
    }

    // The buffer is on the stack, so give it as a reference, which should result in it
    // simply being copied into the atom handle, requiring no allocation.
    if let Ok(s) = str::from_utf8(buf.as_ref()) {
        return mk_atom_final(Cow::Borrowed(s), span);
    }

    // `str::from_utf8` failed to decode the buffer as UTF-8. Re-run the conversion to get the
    // more detailed error type that `String::from_utf8` provides.
    match String::from_utf8(buf.into_vec()) {
        Err(err) => mk_utf8_err(err, span),
        Ok(s) => {
            unreachable!(
                "`str::from_utf8` says the bytes aren't UTF-8, but `String::from_utf8` says they \
                 are!? For reference, the string is {:?}.",
                s
            )
        }
    }
}

fn mk_io_err(err: io::Error, pos: InputPosition) -> Option<Result<InputToken>> {
    Some(Err(ErrorKind::IOError(err, pos).into()))
}

fn mk_utf8_err(err: FromUtf8Error, span: InputSpan) -> Option<Result<InputToken>> {
    Some(Err(ErrorKind::Utf8Error(err, span).into()))
}
