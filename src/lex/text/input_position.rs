use lex::InputResourceDescriptor;

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct InputPosition {
    pub input_resrc: InputResourceDescriptor,
    pub byte_pos: usize,
    pub line: usize,
    // FIXME: Rust chars do not correspond exactly to columns, and bytes are even worse.
    pub column: usize,
}

impl InputPosition {
    pub fn new(
        input_resrc: InputResourceDescriptor,
        byte_pos: usize,
        line: usize,
        column: usize,
    ) -> Self {
        InputPosition {
            input_resrc: input_resrc,
            byte_pos: byte_pos,
            line: line,
            column: column,
        }
    }

    pub fn input_resrc(&self) -> &InputResourceDescriptor {
        &self.input_resrc
    }

    pub fn byte_pos(&self) -> usize {
        self.byte_pos
    }

    pub fn line(&self) -> usize {
        self.line
    }

    pub fn column(&self) -> usize {
        self.column
    }
}
