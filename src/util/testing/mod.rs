#![cfg(test)]

use std::fmt;

pub fn assert_all_eq<I1, I2>(iterator_1: I1, iterator_2: I2)
    where I1: IntoIterator,
          I2: IntoIterator,
          I1::Item: fmt::Debug + PartialEq<I2::Item>,
          I2::Item: fmt::Debug
{
    for (i, (a, b)) in iterator_1.into_iter().zip(iterator_2).enumerate() {
        // "index of iteration" is the term used by the documentation of
        // `std::iter::Iterator::enumerate`.
        assert_eq!(a, b, "at index of iteration {}", i)
    }
}
