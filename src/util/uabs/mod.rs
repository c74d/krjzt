use num::PrimInt;
use std::mem;

const BITS_PER_BYTE: usize = 8;

pub trait UnsignedAbs: PrimInt {
    type Output;

    fn uabs(self) -> <Self as UnsignedAbs>::Output;
}

macro_rules! impl_uabs {
    ($Input:ty, $Output:ty) => {
        impl UnsignedAbs for $Input {
            type Output = $Output;

            fn uabs(self) -> $Output {
                // <https://graphics.stanford.edu/~seander/bithacks.html#IntegerAbs>
                let m = self.signed_shr((mem::size_of::<Self>() * BITS_PER_BYTE - 1) as u32);
                ((self + m) ^ m) as $Output
            }
        }
    }
}

impl_uabs!(i8, u8);
impl_uabs!(i16, u16);
impl_uabs!(i32, u32);
impl_uabs!(i64, u64);
impl_uabs!(isize, usize);
