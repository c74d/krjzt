use super::*;
use lex::InputResourceDescriptor;
use string_cache::DefaultAtom as AtomImpl;

type PosPair = (usize, usize);

#[derive(Debug, Eq, PartialEq)]
enum TupleExpr {
    A(PosPair, PosPair, AtomImpl),
    S(PosPair, PosPair, Vec<TupleExpr>),
}

fn pos_pair(pos: InputPosition) -> PosPair {
    (pos.line, pos.column)
}

fn ie_to_tuple_expr(
    InputExpr {
        value,
        span: InputSpan { start, end },
    }: InputExpr,
) -> TupleExpr {
    match value {
        Expr::Atom(s) => TupleExpr::A(pos_pair(start), pos_pair(end), s),
        Expr::Seq(xs) => {
            TupleExpr::S(
                pos_pair(start),
                pos_pair(end),
                xs.into_iter().map(ie_to_tuple_expr).collect(),
            )
        }
    }
}

fn check(input: &str, check_expr: TupleExpr) {
    assert_eq!(
        expr_from_bytes(input.as_bytes(), InputResourceDescriptor::default())
            .map(ie_to_tuple_expr)
            .unwrap(),
        check_expr
    )
}

macro_rules! a {
    ($start_line:tt : $start_col:tt, $end_line:tt : $end_col:tt, $s:expr) => {
        TupleExpr::A(($start_line, $start_col), ($end_line, $end_col), $s.into())
    }
}

macro_rules! s {
    ($start_line:tt : $start_col:tt, $end_line:tt : $end_col:tt, [$($s:expr,)*]) => {
        TupleExpr::S(($start_line, $start_col), ($end_line, $end_col), vec![$($s,)*])
    }
}

#[test]
fn parse_simple_atom_1() {
    check("a", a!(1:01, 1:01, "a"));
}

#[test]
fn parse_simple_atom_2() {
    check("abc", a!(1:01, 1:03, "abc"));
}

#[test]
fn parse_simple_tuple_1() {
    check("()", s!(1:01, 1:02, []));
}

#[test]
fn parse_simple_tuple_2() {
    check(
        "(a)",
        s!(1:01, 1:03, [
             a!(1:02, 1:02, "a"),
          ]),
    );
}

#[test]
fn parse_simple_tuple_3() {
    check(
        "(abc)",
        s!(1:01, 1:05, [
             a!(1:02, 1:04, "abc"),
          ]),
    );
}

#[test]
fn parse_simple_tuple_4() {
    check(
        "(abc; foo; bar; baz; quux; xyz)",
        s!(1:01, 1:31, [
             a!(1:02, 1:04, "abc"),
             a!(1:07, 1:09, "foo"),
             a!(1:12, 1:14, "bar"),
             a!(1:17, 1:19, "baz"),
             a!(1:22, 1:25, "quux"),
             a!(1:28, 1:30, "xyz"),
          ]),
    );
}

#[test]
fn parse_deep_tuple_1() {
    check(
        "(abc; foo; (bar; baz); quux; xyz)",
        s!(1:01, 1:33, [
             a!(1:02, 1:04, "abc"),
             a!(1:07, 1:09, "foo"),
             s!(1:12, 1:21, [
                a!(1:13, 1:15, "bar"),
                a!(1:18, 1:20, "baz"),
             ]),
             a!(1:24, 1:27, "quux"),
             a!(1:30, 1:32, "xyz"),
          ]),
    );
}
