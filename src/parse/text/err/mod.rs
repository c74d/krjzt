use lalrpop_util;
use lex;
use lex::text::InputPosition;
use lex::text::phases::last::Token;

type LalrpopError = lalrpop_util::ParseError<InputPosition, Token, lex::text::Error>;

error_chain! {
    links {
        Lex(lex::text::Error, lex::text::ErrorKind);
    }

    errors {
        Lalrpop(err: LalrpopError)
    }
}

impl From<LalrpopError> for Error {
    fn from(err: LalrpopError) -> Self {
        // TODO: More specifically map LALRPOP errors into krjzt errors.
        match err {
            lalrpop_util::ParseError::User { error } => error.into(),
            other_err => ErrorKind::Lalrpop(other_err).into(),
        }
    }
}
