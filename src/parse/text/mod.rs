pub use self::err::*;
use lex::InputResourceDescriptor;
use lex::text::InputPosition;
use lex::text::InputSpan;
use lex::text::Result as LexResult;
use lex::text::lex;
use lex::text::phases::last::InputToken;
use lex::text::phases::last::Token;
use std::convert::Into;
use std::io::Read;
use string_cache::DefaultAtom as AtomImpl;

mod err;

#[cfg_attr(rustfmt, rustfmt_skip)]
mod lalrpop_parser;

#[cfg(test)]
mod tests;

type LalrpopTokPosTriple = (InputPosition, Token, InputPosition);
type InputTokenToTripleFn = fn(LexResult<InputToken>) -> LexResult<LalrpopTokPosTriple>;

#[derive(Debug, Eq, PartialEq)]
pub enum Expr {
    /// A basic syntactic unit; e.g., `foo` or `+`.
    Atom(AtomImpl),

    /// An expression composed of one or more expressions; e.g., `8`, `x + y` or `foo (bar baz)
    /// qux`. Notionally a "sequence" of expressions, although it may contain only a single
    /// expression, or no expressions at all (in which case it represents the unit value).
    Seq(Vec<InputExpr>),
}

#[derive(Debug, Eq, PartialEq)]
pub struct InputExpr {
    pub value: Expr,
    pub span: InputSpan,
}

pub fn expr_from_bytes(input: &[u8], input_resrc: InputResourceDescriptor) -> Result<InputExpr> {
    lalrpop_parser::parse_Expr(lex(input.bytes(), input_resrc).map(
        input_token_to_triple as
            InputTokenToTripleFn,
    )).map_err(Into::into)
}

/// Currently consumes the entire iterator because that's what LALRPOP does (see also
/// <https://github.com/nikomatsakis/lalrpop/issues/156>).
pub fn expr_from_tokens<I>(input_iter: I) -> Result<InputExpr>
where
    I: Iterator<Item = LexResult<InputToken>>,
{
    lalrpop_parser::parse_Expr(input_iter.map(
        input_token_to_triple as InputTokenToTripleFn,
    )).map_err(Into::into)
}

fn input_token_to_triple(input: LexResult<InputToken>) -> LexResult<LalrpopTokPosTriple> {
    input.map(|InputToken {
         token,
         span: InputSpan { start, end },
     }| (start, token, end))
}

fn mk_ie(value: Expr, start_pos: InputPosition, end_pos: InputPosition) -> InputExpr {
    InputExpr {
        value: value,
        span: InputSpan::from_endpoints(start_pos, end_pos),
    }
}

fn mk_atom(content: AtomImpl, start_pos: InputPosition, end_pos: InputPosition) -> InputExpr {
    mk_ie(Expr::Atom(content), start_pos, end_pos)
}

fn mk_seq(content: Vec<InputExpr>, start_pos: InputPosition, end_pos: InputPosition) -> InputExpr {
    mk_ie(Expr::Seq(content), start_pos, end_pos)
}

fn mk_seq_or_single(
    mut content: Vec<InputExpr>,
    start_pos: InputPosition,
    end_pos: InputPosition,
) -> InputExpr {
    if content.len() == 1 {
        content.remove(0)
    } else {
        mk_seq(content, start_pos, end_pos)
    }
}
