
about:
	@echo 'About this Makefile:'
	@echo 'This Makefile is for use with the Nix package manager.'
	@echo 'Users of other package managers should use Cargo directly instead.'

build:
	@nix-shell --run 'cargo build'

test:
	@nix-shell --run 'cargo test'

misc-rust-files = \
	build.rs

fmt:
	@ { if [ -z "$$IN_NIX_SHELL" ]; then \
	      nix-shell --run " \
	        cargo-fmt; \
	        rustfmt $(misc-rust-files); \
	      "; \
	    else \
	      cargo-fmt; \
	      rustfmt $(misc-rust-files); \
	    fi } \
	  | { grep -v '^Using rustfmt config file '; true; }

shell:
	@nix-shell
