
extern crate lalrpop;

fn main() {
    lalrpop_phase();
}

fn lalrpop_phase() {
    lalrpop::process_root().expect("Error in LALRPOP!");
}
