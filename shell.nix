let
  pkgs = import <nixpkgs> {};
in

pkgs.stdenv.mkDerivation rec {
  name = "krjzt";

  nativeBuildInputs = with pkgs; [
    cargo
    clang
    rustc
    rustfmt
  ];
}
